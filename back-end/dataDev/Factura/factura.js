/* Modelo de Factura Basico usando Java LocalStorage and SessionStorage 
   Autor: Victor Manuel Cervantes Torres
   TechU - Practicioner 2019
*/

function guardarEnLocalStorage() {
  var codigo = document.getElementById("codigo").value; 
  var fecha = document.getElementById("fecha").value;
  var montoTotal = document.getElementById("montoTotal").value;   
  var valorjson = { "codigo":codigo, "fecha":fecha, "montoTotal": montoTotal };
  localStorage.setItem(codigo , JSON.stringify(valorjson));
}

function leerDeLocalStorage() {
  var codigo = document.getElementById("codigo").value; 
  var datosUsuario = JSON.parse(localStorage.getItem(codigo));
  if (datosUsuario != null)
  {
	document.getElementById("fecha").value= datosUsuario.fecha;
	document.getElementById("montoTotal").value=datosUsuario.montoTotal; 
  }
}
function borrarDeLocalStorage() {
  var codigo = document.getElementById("codigo").value; 
  var info = localStorage.removeItem(codigo);
}
function limpiarLocalStorage() {
  var info = localStorage.clear();
}

