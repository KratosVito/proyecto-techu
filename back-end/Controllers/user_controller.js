//Creador: Victor M. Cervantes Torres
// Descrip: TechU - Practitiioner 2019 - GET/POST/DELETE/PUT (Archivos y Logueo)

//Constantes
const API_LISTENED   = "API escuchando por puerto 3000...";
const MSG            = "Mensaje";
const REG_DES        = "Registro";
const RES_CREATED    = "Recurso creado con exito.";
const RES_DELETED    = "Recurso borrado con exito.";
const RES_FOUND_NOK  = "Recurso(s) no encontrado(s).";
const RES_EXIST_NOK  = "Recurso(s) no existen.";
const RES_LOGIN      = "Login correcto";
const RES_LOGIN_NOK  = "Login incorrecto";
const RES_LOGOUT     = "Logout realizado con exito";
const RES_LOGOUT_DES = "Logout incorrecto";
const RES_LOGOUT_NOK = "Logout incorrecto";
const RES_PARAM_NOK  = "Parametros incorrectos";
const RES_UPDATED    = "Recurso actualizado con exito.";
const ERR_USER_COLL  = "ERROR obteniendo usuario.";
const RES_VOID       = "";
const OP_FAIL        = -1;
const OP_OK          = 0;
const PORT           = process.env.PORT || 3000;
const URL_BASE       = '/apitechu/v1/';
//Esto no deberia de hacerse ni de COÑO
//const apikeyMLab      = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF'
const apikeyMLab      = 'apiKey=' + process.env.MLAB_API_KEY;

const fString        = 'f={"_id":0}&';
const usrTabla       = 'user';
const accTabla       = 'Account';
const traTabla       = 'transaction';

//Variables
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()); //Utiliza Pasrser con formato JSON

var reqJson = require('request-json');
//var baseMLabURL = 'https://api.mlab.com/api/1/databases/apitechuelb5ed/collections/'
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu13db/collections/'
var httpClient  = reqJson.createClient(baseMLabURL);

//Obtencion de todos loa usuarios
function getUsers(req, res){
  httpClient.get(usrTabla + '?' + fString + apikeyMLab,
    function(err,resMLab,body) {
      var response = {};
      if(err){
        response = {MSG:ERR_USER_COLL}
        res.status(500)
      }else {
        if(body.length > 0) response = body
        else {
          response = {MSG:RES_EXIST_NOK}
          res.status(500);
        }
      }
      res.send(response);
    })
}

function getQueryUser(req, res){
  if (Object.keys(req.query).length > 0 && req.query.idUsuario != undefined ){
        var qString = 'q={"idUsuario":' + req.query.idUsuario + '}&';
        console.log(usrTabla + '?' + qString + fString + apikeyMLab);
        httpClient.get(usrTabla + '?' + qString + fString + apikeyMLab,
        function(err,resMLab,body) {
          var response = {};
          if(err){
            response = {MSG:ERR_USER_COLL}
            res.status(500)
          }else {
            if(body.length > 0){
              response = body
            } else {
              response = {MSG:RES_EXIST_NOK}
              res.status(500);
            }
          }
          res.send(response);
        })
      }
      else res.send({MSG:RES_PARAM_NOK});
}

function getUser(req, res){
  var id = req.params.id;
  var qString = 'q={"idUsuario":' + id + '}&';
  console.log(usrTabla + '?' + qString + fString + apikeyMLab);
  httpClient.get(usrTabla + '?' + qString + fString + apikeyMLab,
        function(err,resMLab,body) {
          var response = {};
          if(err){
            console.log(err)
            response = {MSG:ERR_USER_COLL}
            res.status(500)
          }else {
            if(body.length > 0){
              response = body
            } else {
              response = {MSG:RES_EXIST_NOK}
              res.status(500);
            }
          }
          res.send(response);
        })
}

function postUser(req, res){
 var clienteMlab = reqJson.createClient(baseMLabURL);
 console.log(req.body);
 clienteMlab.get('user?' + apikeyMLab,
   function(error, respuestaMLab, body){
     var newID = body.length + 1;
     console.log("newID:" + newID);
     var newUser = {
       "idUsuario" : newID,
       "Nombre"    : req.body.Nombre,
       "Apellido"  : req.body.Apellido,
       "email"     : req.body.email,
       "clave"     : req.body.clave
     };

     clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
       function(error, respuestaMLab, body){
         console.log(body);
         res.status(201);
         res.send(body);
       });
   });
}

function putUser(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"idUsuario":' + id + '}&';
 var clienteMlab = reqJson.createClient(baseMLabURL);
 clienteMlab.get('user?'+ queryStringID + apikeyMLab,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    clienteMlab.put(baseMLabURL +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body.n); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
}

function putUsers(req, res) {
  var id = req.params.id;
  let userBody = req.body;
  var queryString = 'q={"idUsuario":' + id + '}&';
  var httpClient = reqJson.createClient(baseMLabURL);
  console.log("URL", 'user?' + queryString + apikeyMLab);
  httpClient.get('user?' + queryString + apikeyMLab,
    function(err, respuestaMLab, body){
      let response = body[0];
      console.log("cuerpo:", body);
      //Actualizo campos del usuario
      let updatedUser = {
        "idUsuario" : req.body.idUsuario,
        "Nombre" : req.body.Nombre,
        "Apellido" : req.body.Apellido,
        "email" : req.body.email,
        "clave" : req.body.clave
      };//Otra forma simplificada (para muchas propiedades)
      // var updatedUser = {};
      // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
      // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
      // PUT a mLab
      console.log(response);
      httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              response = {
                "msg" : "Error actualizando usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario actualizado correctamente."
              }
              res.status(200);
            }
          }
          res.send(response);
        });
    });
}

function deleteUser(req, res){
  var id = req.params.id;
  var queryStringID = 'q={"idUsuario":' + id + '}&';
  console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
  var httpClient = reqJson.createClient(baseMLabURL);
  httpClient.get('user?' +  queryStringID + apikeyMLab,
    function(error, respuestaMLab, body){
      var respuesta = body[0];
      httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
        function(error, respuestaMLab,body){
          res.send(body);
      });
    });
}

function login(req, res){
  // console.log("POST /colapi/v3/login");
  let email = req.body.email;
  let pass = req.body.clave;
  let queryString = 'q={"email":"' + email + '","clave":"' + pass + '"}&';
  let limFilter = 'l=1&';
  let clienteMlab = reqJson.createClient(baseMLabURL);
  clienteMlab.get('user?'+ queryString + limFilter + apikeyMLab,
    function(error, respuestaMLab, body) {
      if(!error) {
        if (body.length == 1) { // Existe un usuario que cumple 'queryString'
          let login = '{"$set":{"logged":true}}';
          clienteMlab.put('user?q={"idUsuario": ' + body[0].idUsuario + '}&' + apikeyMLab, JSON.parse(login),
          //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
            function(errPut, resPut, bodyPut) {
              res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].idUsuario});
              // If bodyPut.n == 1, put de mLab correcto
            });
        }
        else {
          res.status(404).send({"msg":"Usuario no válido."});
        }
      } else {
        res.status(500).send({"msg": "Error en petición a mLab."});
      }
  });
}

function logout(req, res){
  // console.log("POST /colapi/v3/login");
  let email = req.body.email;
  let queryString = 'q={"email":"' + email + '"}&';
  let limFilter = 'l=1&';
  let clienteMlab = reqJson.createClient(baseMLabURL);
  clienteMlab.get('user?'+ queryString + limFilter + apikeyMLab,
    function(error, respuestaMLab, body) {
      if(!error) {
        if (body.length == 1) { // Existe un usuario que cumple 'queryString'
          let logout = '{"$unset":{"logged":true}}';
          clienteMlab.put('user?q={"idUsuario": ' + body[0].idUsuario + '}&' + apikeyMLab, JSON.parse(logout),
          //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
            function(errPut, resPut, bodyPut) {
              res.send({'msg':'logout correcto', 'user':body[0].email, 'userid':body[0].idUsuario});
              // If bodyPut.n == 1, put de mLab correcto
            });
        }
        else {
          res.status(404).send({"msg":"Usuario no válido."});
        }
      } else {
        res.status(500).send({"msg": "Error en petición a mLab."});
      }
  });
}

module.exports.getUsers     = getUsers;
module.exports.getQueryUser = getQueryUser;
module.exports.getUser      = getUser;
module.exports.postUser     = postUser;
module.exports.putUser      = putUser;
module.exports.putUsers     = putUsers;
module.exports.deleteUser   = deleteUser;
module.exports.login        = login;
module.exports.logout       = logout;
