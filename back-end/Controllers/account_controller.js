//Creador: Victor M. Cervantes Torres
// Descrip: TechU - Practitiioner 2019 - GET/POST/DELETE/PUT (Archivos y Logueo)

//Constantes
const API_LISTENED   = "API escuchando por puerto 3000...";
const MSG            = "Mensaje";
const REG_DES        = "Registro";
const RES_CREATED    = "Recurso creado con exito.";
const RES_DELETED    = "Recurso borrado con exito.";
const RES_FOUND_NOK  = "Recurso(s) no encontrado(s).";
const RES_EXIST_NOK  = "Recurso(s) no existen.";
const RES_LOGIN      = "Login correcto";
const RES_LOGIN_NOK  = "Login incorrecto";
const RES_LOGOUT     = "Logout realizado con exito";
const RES_LOGOUT_DES = "Logout incorrecto";
const RES_LOGOUT_NOK = "Logout incorrecto";
const RES_PARAM_NOK  = "Parametros incorrectos";
const RES_UPDATED    = "Recurso actualizado con exito.";
const ERR_USER_COLL  = "ERROR obteniendo usuario.";
const RES_VOID       = "";
const OP_FAIL        = -1;
const OP_OK          = 0;
const PORT           = process.env.PORT || 3000;
const URL_BASE       = '/apitechu/v1/';
//Esto no deberia de hacerse ni de COÑO
//const apikeyMLab      = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF'
const apikeyMLab      = 'apiKey=' + process.env.MLAB_API_KEY;

const fString        = 'f={"_id":0}&';
const usrTabla       = 'user';
const accTabla       = 'Account';
const traTabla       = 'transaction';

//Variables
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()); //Utiliza Pasrser con formato JSON

var reqJson = require('request-json');
//var baseMLabURL = 'https://api.mlab.com/api/1/databases/apitechuelb5ed/collections/'
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu13db/collections/'
var httpClient  = reqJson.createClient(baseMLabURL);

function getAccounts(req, res){
  var id = req.params.id;
  var qString = 'q={"idUsuario":' + id + '}&';
  console.log(usrTabla + '?' + qString + fString + apikeyMLab);
  httpClient.get(usrTabla + '?' + qString + fString + apikeyMLab,
        function(err,resMLab,body) {
          var response = {};
          if(err){
            console.log(err)
            response = {MSG:ERR_USER_COLL}
            res.status(500)
          }else {
            if(body.length > 0){
              response = body
            } else {
              response = {MSG:RES_EXIST_NOK}
              res.status(500);
            }
          }
          res.send(response);
        })
}

module.exports.getAccounts = getAccounts;
