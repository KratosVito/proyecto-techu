// Creador: Victor M. Cervantes Torres
// Descrip: TechU - Practitiioner 2019 - GET/POST/DELETE/PUT (Archivos y Logueo)
//
// Data para Prueba:
//  Login 1 (POST): {"email":"rocodihie1@cornell.edu","clave":"C7Ykwo"}
//  Login 2 (POST): {"email":"gmichelottik@netscape.com","clave":"2fomLGONHg"}
//  Logout (POST): {"email":"rocodihie1@cornell.edu"}
//  Usuarios Logueados (GET): http://localhost:3000/apitechu/v1/logins
//  Lista por Rango: http://localhost:3000/apitechu/v1/usersq/?numReg=3&posIni=8
//
//Comentarios Varios
// Generar Comentarios de Codigo ya existente : Ctrl + Shift + /
//COMANDO PARA LIBERAR EL PUERTO3000 CUANDO LA API NO ARRANQUE fuser -k 3000/tcp
//Callback Funcion anonima que se inserta como parametro de otra funcion
//logged = Propiedad temporal creada en tiempo de ejecucion
//
//Constantes
const API_LISTENED   = "API escuchando por puerto 3000...";
const FILE_DIR       = "./dataDev/";
const FILE_WRITE     = "Datos escritos en Archivo";
const MSG            = "Mensaje";
const REG_DES        = "Registro";
const RES_CREATED    = "Recurso creado con exito.";
const RES_DELETED    = "Recurso borrado con exito.";
const RES_FOUND_NOK  = "Recurso(s) no encontrado(s).";
const RES_EXIST_NOK  = "Recurso(s) no existen.";
const RES_LOGIN      = "Login correcto";
const RES_LOGIN_NOK  = "Login incorrecto";
const RES_LOGOUT     = "Logout realizado con exito";
const RES_LOGOUT_DES = "Logout incorrecto";
const RES_LOGOUT_NOK = "Logout incorrecto";
const RES_PARAM_NOK  = "Parametros incorrectos";
const RES_UPDATED    = "Recurso actualizado con exito.";
const ERR_USER_COLL  = "ERROR obteniendo usuario.";
const RES_VOID       = "";
const OP_FAIL        = -1;
const OP_OK          = 0;
const PORT           = process.env.PORT || 3000;

//Configuracion desde archivo .env
const user_controller = require('./Controllers/user_controller')
const account_controller = require('./Controllers/account_controller')

const apikeyMLab      = 'apiKey=' + process.env.MLAB_API_KEY;
const baseMLabURL     = process.env.MLAB_URL
const URL_BASE        = process.env.URL_BASE

require('dotenv').config

const fString        = 'f={"_id":0}&';
const usrTabla       = 'user';
const accTabla       = 'Account';
const traTabla       = 'transaction';

//Variables
var express          = require('express');
var app              = express();
var fileUser         = FILE_DIR + 'user.json'
var userFile         = require(fileUser);
var bodyParser       = require('body-parser');

app.use(bodyParser.json()); //Utiliza Pasrser con formato JSON

var reqJson = require('request-json');
var httpClient  = reqJson.createClient(baseMLabURL);

//Obtencion de todos los usuarios
app.get(URL_BASE + 'users', user_controller.getUsers);

//Consulta datos de usuario via Query por idUsuario
app.get(URL_BASE + 'usersq', user_controller.getQueryUser);

//Consulta datos de usuario por idUsuario
app.get(URL_BASE + 'users/:id', user_controller.getUser);

app.get(URL_BASE + 'users/:id/accounts', account_controller.getAccounts);

// POST 'users' mLab
app.post(URL_BASE + 'users', user_controller.postUser);

//PUT users con parámetro 'id'
app.put(URL_BASE + 'users/:id', user_controller.putUser);

// Petición PUT con id de mLab (_id.$oid)
app.put(URL_BASE + 'usersmLab/:id',user_controller.putUsers);

 //DELETE user with id
app.delete(URL_BASE + "users/:id", user_controller.deleteUser);

//Method POST login
app.post(URL_BASE + "login", user_controller.login);

//Method POST logout
app.post(URL_BASE + "logout", user_controller.logout);

//Funcion Listen: Peticion de escucha en Puerto 3000
app.listen(PORT,function(){console.log(API_LISTENED)});

// //Lista todos los 'users' (Collections)
// app.get(URL_BASE + 'users', function(req, res){res.send(userFile);});
//
// //Peticion Get de un 'user' (Instance)
// app.get(URL_BASE + 'users/:id',
//   function(req, res){
//     let cnt = searchID(req.params.id)
//     if (cnt != OP_FAIL) res.send(userFile[cnt])
//     else res.send({MSG:RES_FOUND_NOK})
//   });
//
// //Parametros por query se tratan como un Json
// //app.get(URL_BASE + 'usersq',function(req, res) {res.send(req.query);});
// // for (const key in req.query) {console.log(key, req.query[key])}
// app.get(URL_BASE + 'usersq',
//   function(req, res) {
//     if (Object.keys(req.query).length > 0 &&
//         req.query.posIni != undefined &&
//         req.query.numReg != undefined &&
//         isNaN(req.query.posIni) == false &&
//         isNaN(req.query.numReg) == false
//     ){
//           let posIni = parseInt(req.query.posIni) || 0;
//           let posFin = (parseInt(req.query.numReg) || 1) + posIni;
//           res.send(userFile.slice(posIni,posFin));
//     }
//     else res.send({MSG:RES_PARAM_NOK});
//   });
//
// //Crear un Usuario se envia por el Body el Nombre/Apellido/email/clave
// //El Body se genera como un JSON, y se guarda en el fileUser
// app.post(URL_BASE + 'users' ,
//     function(req,res){
//       if (Object.keys(req.body).length > 0){
//         let newUser = {
//           idUsuario : userFile[userFile.length-1].idUsuario + 1,
//           Nombre   : req.body.Nombre,
//           Apellido : req.body.Apellido,
//           email    : req.body.email,
//           clave    : req.body.clave
//         }
//
//         userFile.push(newUser);
//         if (writeUserDataToFile(userFile) == OP_OK){
//             res.send({MSG : RES_CREATED,REG_DES: newUser});
//             res.status(201);
//         }
//       }
//       else res.send({MSG : RES_PARAM_NOK});
//     });
//
// //Borrar un Usuario se envia URI
// app.delete(URL_BASE + 'users/:id',
//     function(req,res){
//       let cnt = searchID(req.params.id)
//       if (cnt != OP_FAIL){
//         tmp = userFile.splice(cnt,1)
//         if (writeUserDataToFile(userFile) == OP_OK){
//           res.send({MSG : RES_DELETED,REG_DES:tmp});
//           res.status(204);
//         }
//       }
//       else res.send({MSG:RES_FOUND_NOK})
//     });
//
// //Actualiza un Usuario se envia por el Body el Nombre/Apellido/email/clave
// //El Body se genera como un JSON, y se guarda en el fileUser
// app.put(URL_BASE + 'users/:id',
//     function(req,res){
//       if (Object.keys(req.body).length > 0){
//         let cnt = searchID(req.params.id)
//         if (cnt != OP_FAIL){
//           userFile[cnt].Nombre   = req.body.Nombre;
//           userFile[cnt].Apellido = req.body.Apellido;
//           userFile[cnt].email    = req.body.email;
//           userFile[cnt].clave    = req.body.clave;
//           if (writeUserDataToFile(userFile) == OP_OK){
//             res.send({MSG : RES_UPDATED, REG_DES : userFile[cnt]});
//             res.status(201);
//           }
//         }
//         else res.send({MSG:RES_FOUND_NOK})
//       }
//       else res.send({MSG:RES_PARAM_NOK});
//     });
//
// //Login de Usuario via Body se envia email y clave, se activa propiedad logged
// app.post(URL_BASE + 'login' ,
//     function(req,res){
//       if (Object.keys(req.body).length > 0){
//         let cnt = searchLogin(req.body.email,req.body.clave)
//         if (cnt != OP_FAIL){
//           userFile[cnt].logged = true;
//           if (writeUserDataToFile(userFile) == OP_OK){
//             res.send({MSG : RES_LOGIN, REG_DES : userFile[cnt]});
//           }
//         }
//         else res.send({MSG:RES_LOGIN_NOK})
//       }
//       else res.send({MSG:RES_PARAM_NOK});
//     });
//
// //Logout de Usuairo via Body se envia email, se borra propiedad logged
// app.post(URL_BASE + 'logout' ,
//     function(req,res){
//       if (Object.keys(req.body).length > 0){
//         let cnt = searchLogout(req.body.email)
//         if (cnt != OP_FAIL){
//           if (userFile[cnt].logged){
//             delete userFile[cnt].logged;
//             if (writeUserDataToFile(userFile) == OP_OK)
//               res.send({MSG:RES_LOGOUT});
//           }
//           else
//             res.send({MSG:RES_LOGOUT_DES});
//         }
//         else res.send({MSG:RES_LOGOUT_NOK})
//       }
//       else res.send({MSG:RES_PARAM_NOK});
//     });
//
// //Obtener todos los Usuarios que esten Logados, Salida JSON con Logados
// //Peticion Get logins
// app.get(URL_BASE + 'logins',
//   function(req, res){
//     let jsonTmpVector = [];
//     for (x of userFile){
//       if(x.logged)
//         jsonTmpVector.push(x);
//     }
//     if (jsonTmpVector.length > 0)
//       res.send(jsonTmpVector);
//     else
//       res.send({MSG:RES_VOID});
// });
//
// function writeUserDataToFile(data) {
//    var fs = require('fs');
//    var jsonUserData = JSON.stringify(data);
//
//    fs.writeFile(fileUser, jsonUserData, "utf8",
//     function(err) { //función manejadora para gestionar errores de escritura
//       if(err) {
//         console.log(err);
//         return OP_FAIL;
//       }
//       else console.log(FILE_WRITE);
//     })
//     return OP_OK;
//  }
//
// //Parametros por query se tratan como un Json
// //Consulta por Apellido = datBu1
// //Los registros que contengan ejemplo "re": Moreno/Perez/Flores
// //se envia Numero de Registro y Paginacion
//  app.get(URL_BASE + 'usersp',
//    function(req, res) {
//      for (const key in req.query) {console.log(key, req.query[key])}
//      if (Object.keys(req.query).length > 0 &&
//          req.query.datBu1 != undefined &&
//          req.query.numReg != undefined &&
//          req.query.numPag != undefined &&
//          isNaN(req.query.datBu1) == true  &&
//          isNaN(req.query.numPag) == false &&
//          isNaN(req.query.numReg) == false
//      ){
//            let datBu1 = req.query.datBu1
//            let numPag = parseInt(req.query.numPag) || 1;
//            let numReg = (parseInt(req.query.numReg) || 1);
//            let lngFile = userFile.length
//            let idx = 0
//            let numTotReg = numReg*numPag
//            let jsonTmpVector = [];
//            let numRegOk = 0
//
//            for (idx = 0; idx < lngFile, numRegOk <= numTotReg;idx++){
//              if(RegExp(datBu1).test(userFile[idx].Apellido)){
//                numRegOk++;
//                if (numRegOk >= (numTotReg - numReg + 1 ) &&
//                    numRegOk <= numTotReg)
//                   jsonTmpVector.push(idx);
//              }
//            }
//            if (numRegOk > 0)
//              res.send(jsonTmpVector);
//            else
//              res.send({MSG:RES_VOID});
//      }
//      else res.send({MSG:RES_PARAM_NOK});
//    });
//
// function searchID(idx){
//   for(x in userFile)
//     if (userFile[x].idUsuario == idx)
//       return x;
//   return OP_FAIL;
// }
//
// function searchLogout(email){
//   for(x in userFile)
//     if (userFile[x].email == email)
//       return x;
//   return OP_FAIL;
// }
//
// function searchLogin(email,clave){
//   for(x in userFile)
//     if (userFile[x].email == email && userFile[x].clave == clave)
//       return x;
//   return OP_FAIL;
// }
